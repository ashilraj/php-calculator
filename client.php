<?php

namespace Calculator;

require_once './autoloader.php';

$operators_to_commands = [
    '+' => 'Calculator\AddCommand',
    '-' => 'Calculator\SubtractCommand',
    'x' => 'Calculator\MultiplyCommand',
    '/' => 'Calculator\DivideCommand'
];


if(isset($argv[3]) && isset($operators_to_commands[$argv[3]])) {

    validateInputs([$argv[1], $argv[2]]);
    
    $calculator = new Calculator($argv[1], $argv[2]);

    $invoker = new CommandInvoker(new $operators_to_commands[$argv[3]]($calculator));

    $output = $invoker->handle();
    echo $output . "\n";
}

// validate against XSS & SQL injection

function validateInputs($inputs)
{
    foreach($inputs as $item)
    {
        if (filter_var($item, FILTER_VALIDATE_INT) == false) {
            if (filter_var($item, FILTER_VALIDATE_FLOAT) == false && $item != 0) {
                throw new \InvalidArgumentException('Unprocessable Entity');
            }
        }
    }
  
}