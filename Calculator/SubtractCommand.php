<?php

/**
 * Class SubtractCommand
 *
 * Concrete command (SubtractCommand) implement command interface
 *
 */

namespace Calculator;

use Calculator\Calculator;

class SubtractCommand implements CalculatorCommandInterface
{
    private $calculator;

    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }

    public function execute()
    {
        return $this->calculator->subtract();
    }
}