<?php
/**
 * Class CommandInvoker
 *
 * Command invoker set commands and execute them
 *
 */

namespace Calculator;

use Calculator\CalculatorCommandInterface;

class CommandInvoker
{
    private $command;

    public function __construct(CalculatorCommandInterface $command)
    {
        $this->command = $command;
    }

    public function handle()
    {
        return $this->command->execute();
    }
}