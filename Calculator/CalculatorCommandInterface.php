<?php

/**
 * Interface CalculatorCommandInterface
 *
 * The command interface contains just one method execute that need to be implemented
 * in every concrete command
 *
 */

namespace Calculator;

interface CalculatorCommandInterface
{
    public function execute();
}