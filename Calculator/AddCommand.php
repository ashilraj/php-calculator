<?php
/**
 * Class AddCommand
 *
 * Concrete command (AddCommand) implement command interface
 * injects the receiver class (Calculator) to access the
 * real operation, in this case (add) operation
 *
 */

namespace Calculator;

use Calculator\Calculator;

class AddCommand implements CalculatorCommandInterface
{
    private $calculator;

    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }

    public function execute()
    {
        return $this->calculator->add();
    }
}