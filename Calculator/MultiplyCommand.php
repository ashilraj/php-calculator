<?php

/**
 * Class MultiplyCommand
 *
 * Concrete command (MultiplyCommand) implement command interface
 *
 */

namespace Calculator;

use Calculator\Calculator;

class MultiplyCommand implements CalculatorCommandInterface
{
    private $calculator;

    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }

    public function execute()
    {
        return $this->calculator->multiply();
    }
}