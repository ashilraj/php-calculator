<?php

/**
 * Class DivideCommand
 *
 * Concrete command (DivideCommand) implement command interface
 *
 */

namespace Calculator;

use Calculator\Calculator;

class DivideCommand implements CalculatorCommandInterface
{
    private $calculator;

    public function __construct(Calculator $calculator)
    {
        $this->calculator = $calculator;
    }

    public function execute()
    {
        return $this->calculator->divide();
    }
}